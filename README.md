## Table of Contents

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 orderedList:0 -->
- [Installation](#installation)
	- [Prerequisite](#prerequisite)
	- [Dependencies](#dependencies)
	- [Database](#database)
- [Tests](#tests)
- [Up and Running](#up-and-running)
- [Services](#services)
- [License](#license)
<!-- /TOC -->

## Installation

### Prerequisite

This application use Redis for Session storage and background workers
```bash
$ sudo docker volume create redis_data
$ sudo docker run -d -p 6379:6379 --name redis -v redis_data:/data redis:latest
```

### Dependencies

```bash
evopark$ bundle install
```

### Database

```bash
evopark$ bundle exec rake db:create
evopark$ bundle exec rake db:migrate 
```

## Tests

```bash
evopark$ bundle exec rubocop 
evopark$ bundle exec rspec
```

## Up and Running

Starten von Sidekiq
```bash
evopark$ bundle exec sidekiq -C config/sidekiq.yml
```

Starten von Rails
```bash
evopark$ bundle exec rails s -e development
```

## Services

There are some monitoring servives available

[Sidekiq](http://localhost:3000/sidekiq)

## License

MIT License. Copyright 2018 Freistrom. https://freistrom.io

You are not granted rights or licenses to the trademarks of Freistrom, including without limitation the Freistrom name or logo.
