# frozen_string_literal: true

class CreateReminders < ActiveRecord::Migration[5.2]
  def change
    create_table :reminders do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.integer :day_of_month, null: false
      t.time :time_of_day, null: false
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
