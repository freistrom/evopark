# frozen_string_literal: true

# FactoryBot Inclusions
RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  FactoryBot.use_parent_strategy = true
end
