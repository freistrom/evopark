# frozen_string_literal: true

# This will guess the User class
FactoryBot.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :user do
    email { generate :email }
    password { 'password' }
    password_confirmation { 'password' }
    factory :user_with_reminders do
      transient do
        reminders_count { 5 }
      end
      after(:create) do |user, evaluator|
        create_list(:reminder, evaluator.reminders_count, user: user)
      end
    end
  end
end
