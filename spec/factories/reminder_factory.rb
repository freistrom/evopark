# frozen_string_literal: true

# This will guess the User class
FactoryBot.define do
  factory :reminder do
    title { 'John' }
    description { 'Doe' }
    day_of_month { 1 }
    time_of_day { Time.now }
    association :user
  end
end
