# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Reminders', type: :request do
  describe 'GET /reminders' do
    let(:user) { create(:user_with_reminders, reminders_count: 1) }

    it 'return status code 200 OK' do
      user.confirm && sign_in(user)
      get reminders_path
      expect(response).to have_http_status(200)
    end
  end
end
