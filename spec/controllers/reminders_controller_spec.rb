# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RemindersController, type: :controller do
  describe 'GET #index' do
    let(:user) { build(:user) }

    context 'with signed in user' do
      it 'returns a success response' do
        user.confirm && sign_in(user)
        get :index
        expect(response).to be_successful
      end

      it 'returns status code 200 OK' do
        user.confirm && sign_in(user)
        get :index
        expect(response.status).to eq(200)
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with signed out user' do
      it 'returns status code 302 REDIRECT' do
        get :index
        expect(response).to be_redirect
        expect(response.status).to eq(302)
        expect(response).to have_http_status(:redirect)
      end
    end

    it 'returns a text/html' do
      get :index
      expect(response.content_type).to eq('text/html')
    end
  end

  describe 'GET #show' do
    let(:user) { create(:user_with_reminders, reminders_count: 1) }

    context 'with signed in user' do
      it 'returns a success response' do
        user.confirm && sign_in(user)
        get :show, params: { id: user.reminders.first.id }
        expect(response).to be_successful
      end

      it 'returns status code 200 OK' do
        user.confirm && sign_in(user)
        get :show, params: { id: user.reminders.first.id }
        expect(response.status).to eq(200)
        expect(response).to have_http_status(:ok)
      end

      it 'returns a text/html' do
        user.confirm && sign_in(user)
        get :show, params: { id: user.reminders.first.id }
        expect(response.content_type).to eq('text/html')
      end
    end

    context 'with signed out user' do
      it 'returns status code 302 REDIRECT' do
        get :show, params: { id: user.reminders.first.id }
        expect(response).to be_redirect
        expect(response.status).to eq(302)
        expect(response).to have_http_status(:redirect)
      end
    end
  end

  describe 'GET #new' do
    let(:user) { build(:user) }

    context 'with signed in user' do
      it 'returns a success response' do
        user.confirm && sign_in(user)
        get :new
        expect(response).to be_successful
      end

      it 'returns status code 200 OK' do
        user.confirm && sign_in(user)
        get :new
        expect(response.status).to eq(200)
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with signed out user' do
      it 'returns status code 302 REDIRECT' do
        get :new
        expect(response).to be_redirect
        expect(response.status).to eq(302)
        expect(response).to have_http_status(:redirect)
      end
    end

    it 'returns a text/html' do
      get :new
      expect(response.content_type).to eq('text/html')
    end
  end

  describe 'GET #edit' do
    let(:user) { build(:user) }
    let(:reminder) { create(:reminder) }

    context 'with signed in user' do
      it 'returns a success response' do
        user.confirm && sign_in(user)
        get :edit, params: { id: reminder.id }
        expect(response).to be_successful
      end

      it 'returns status code 200 OK' do
        user.confirm && sign_in(user)
        get :edit, params: { id: reminder.id }
        expect(response.status).to eq(200)
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with signed out user' do
      it 'returns status code 302 REDIRECT' do
        get :edit, params: { id: reminder.id }
        expect(response).to be_redirect
        expect(response.status).to eq(302)
        expect(response).to have_http_status(:redirect)
      end
    end

    it 'returns a text/html' do
      get :edit, params: { id: reminder.id }
      expect(response.content_type).to eq('text/html')
    end
  end

  describe 'POST #create' do
    let(:user) { create(:user) }

    context 'with valid params' do
      let(:reminder_params) { attributes_for(:reminder) }

      it 'creates a new Reminder' do
        user.confirm && sign_in(user)
        expect do
          post :create, params: { reminder: reminder_params }
        end.to change(Reminder, :count).by(1)
      end

      it 'redirects to the created reminder' do
        user.confirm && sign_in(user)
        post :create, params: { reminder: reminder_params }
        expect(response).to redirect_to(Reminder.last)
      end
    end

    context 'with invalid params' do
      let(:reminder_params) { attributes_for(:reminder, time_of_day: nil) }

      it "returns a success response (i.e. to display the 'new' template)" do
        user.confirm && sign_in(user)
        post :create, params: { reminder: reminder_params }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    let(:user) { create(:user_with_reminders, reminders_count: 1) }

    context 'with valid params' do
      let(:reminder_params) { attributes_for(:reminder) }

      it 'updates the requested reminder' do
        user.confirm && sign_in(user)
        put :update, params: { id: user.reminders.first.id, reminder: reminder_params }
        user.reminders.first.reload
      end

      it 'redirects to the reminder' do
        user.confirm && sign_in(user)
        put :update, params: { id: user.reminders.first.id, reminder: reminder_params }
        expect(response).to redirect_to(user.reminders.first)
      end
    end

    context 'with invalid params' do
      let(:reminder_params) { attributes_for(:reminder, time_of_day: nil) }

      it "returns a success response (i.e. to display the 'edit' template)" do
        user.confirm && sign_in(user)
        put :update, params: { id: user.reminders.first.id, reminder: reminder_params }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:user) { create(:user_with_reminders, reminders_count: 1) }

    it 'destroys the requested reminder' do
      user.confirm && sign_in(user)
      expect do
        delete :destroy, params: { id: user.reminders.first.id }
      end.to change(Reminder, :count).by(-1)
    end

    it 'redirects to the reminders list' do
      user.confirm && sign_in(user)
      delete :destroy, params: { id: user.reminders.first.id }
      expect(response).to redirect_to(reminders_url)
    end
  end
end
