# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'reminders/show', type: :view do
  before(:each) do
    @reminder = assign(:reminder, create(:user_with_reminders, reminders_count: 1).reminders.first)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Title/)
  end
end
