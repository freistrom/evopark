# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'reminders/edit', type: :view do
  before(:each) do
    @reminder = assign(:reminder, create(:user_with_reminders, reminders_count: 1).reminders.first)
  end

  it 'renders the edit reminder form' do
    render

    assert_select 'form[action=?][method=?]', reminder_path(@reminder), 'post' do
      assert_select 'input[name=?]', 'reminder[title]'

      assert_select 'textarea[name=?]', 'reminder[description]'

      assert_select 'select[name=?]', 'reminder[day_of_month]'
    end
  end
end
