# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'reminders/new', type: :view do
  before(:each) do
    assign(:reminder, create(:user_with_reminders, reminders_count: 1).reminders.first)
  end

  it 'renders new reminder form' do
    pending 'not yet implemented'
    render

    assert_select 'form[action=?][method=?]', reminders_path, 'post' do
      assert_select 'input[name=?]', 'reminder[title]'

      assert_select 'textarea[name=?]', 'reminder[description]'

      assert_select 'select[name=?]', 'reminder[day_of_month]'
    end
  end
end
