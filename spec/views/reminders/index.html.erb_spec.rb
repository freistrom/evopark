# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'reminders/index', type: :view do
  before(:each) do
    assign(:reminders, create(:user_with_reminders, reminders_count: 2).reminders)
  end

  it 'renders a list of reminders' do
    render
    assert_select 'tr>td', text: 'John', count: 2
  end
end
