# frozen_string_literal: true

# Reminde User at a specified Day of the month and a Time at that day
class RemindersMailer < ApplicationMailer
  def reminder_email(to, title, msg)
    @msg = msg
    mail(to: to, subject: title)
  end
end
