# frozen_string_literal: true

json.extract! reminder, :id, :title, :description, :day_of_month, :time_of_day, :created_at, :updated_at
json.url reminder_url(reminder, format: :json)
