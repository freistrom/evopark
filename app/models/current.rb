# frozen_string_literal: true

# app/models/current.rb
class Current < ActiveSupport::CurrentAttributes
  attribute :user
end
