# frozen_string_literal: true

# Reminder Model
class Reminder < ApplicationRecord
  belongs_to :user

  validates :title, presence: true
  validates :description, presence: true
  validates :day_of_month, presence: true, numericality: { only_integer: true }, inclusion: { in: 1..31 }
  validates :time_of_day, presence: true

  after_save do
    Sidekiq::Cron::Job.create(
      name: "reminder_#{id}",
      cron: cron,
      class: 'SchedulerWorker',
      args: [ user.email, title, description ]
    )
  end

  after_destroy do
    Sidekiq::Cron::Job.destroy "reminder_#{id}"
  end

  scope :by_owner, -> { where(user_id: Current.user.id) }

  private

  def cron
    "#{time_of_day.min} #{time_of_day.hour} #{day_of_month} * *"
  end
end
