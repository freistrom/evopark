# frozen_string_literal: true

class User < ApplicationRecord
  devise :two_factor_authenticatable,
         :confirmable, :lockable, :timeoutable, :trackable, :omniauthable,
         :registerable, :recoverable, :rememberable, :validatable

  devise otp_secret_encryption_key: ENV['2FA_ENCRYPTION_KEY']

  has_many :reminders
end
