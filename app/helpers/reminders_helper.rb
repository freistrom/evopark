# frozen_string_literal: true

module RemindersHelper
  def options_for_day_select
    Array.new(31) do |day|
      case day
      when 0, 1, 2
        ["#{(day + 1).ordinalize} Day of Month", day + 1]
      when 28, 29
        ["#{(31 - day).ordinalize} last Day of Month", day + 1]
      when 30
        ['Last Day of Month', day + 1]
      else
        [(day + 1).to_s, day + 1]
      end
    end
  end
end
