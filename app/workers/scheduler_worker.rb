# frozen_string_literal: true

# Reminder Background Worker
class SchedulerWorker
  include Sidekiq::Worker
  def perform(email, title, message)
    RemindersMailer.reminder_email(email, title, message).deliver_now
  end
end
