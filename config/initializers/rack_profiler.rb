# frozen_string_literal: true

if Rails.env.development?
  require 'rack-mini-profiler'

  # initialization is skipped so trigger it
  Rack::MiniProfilerRails.initialize!(Rails.application)
  # Rack::MiniProfiler.config.authorization_mode = :whitelist
  # Rack::MiniProfiler.config.disable_caching = false

  # set RedisStore
  if Rails.env.production?
    uri = URI.parse(ENV['REDIS_URL'])
    Rack::MiniProfiler.config.storage_options = {
      host: uri.host,
      port: uri.port
      # password: uri.password
    }
    Rack::MiniProfiler.config.storage = Rack::MiniProfiler::RedisStore
  end
end
