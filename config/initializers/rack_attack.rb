# frozen_string_literal: true

Rack::Attack.blocklist('block all access to admin') do |request|
  # Requests are blocked if the return value is truthy
  request.path.start_with?('/')
end

Rack::Attack.blocklist('block bad UA logins') do |req|
  req.path == '/login' && req.post? && req.user_agent == 'BadUA'
end

Rack::Attack.blocklist('fail2ban pentesters') do |req|
  Rack::Attack::Fail2Ban.filter(
    "pentesters-#{req.ip}",
    maxretry: 3,
    findtime: 10.minutes,
    bantime: 5.minutes
  ) do
    CGI.unescape(req.query_string) =~ %r{/etc/passwd} ||
      req.path.include?('/etc/passwd') ||
      req.path.include?('wp-admin') ||
      req.path.include?('wp-login')
  end
end

Rack::Attack.blocklist('allow2ban login scrapers') do |req|
  # `filter` returns false value if request is to your login page (but still
  # increments the count) so request below the limit are not blocked until
  # they hit the limit.  At that point, filter will return true and block.
  Rack::Attack::Allow2Ban.filter(
    req.ip,
    maxretry: 20,
    findtime: 1.minute,
    bantime: 1.hour
  ) do
    # The count for the IP is incremented if the return value is truthy.
    req.path == '/login' && req.post?
  end
end
