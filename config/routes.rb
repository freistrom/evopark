# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'reminders#index'

  resources :reminders
  devise_for :users

  authenticate :user, ->(u) { u.current_sign_in_ip? } do
    require 'sidekiq/web'
    require 'sidekiq/cron/web'
    mount Sidekiq::Web => '/sidekiq'
    mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
  end
end
